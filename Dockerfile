FROM quay.io/odh-jupyterhub/jupyterhub-img:3.0.7-b7db22b

WORKDIR /opt/app-root/src/.jupyter

COPY jupyterhub_config.py .
